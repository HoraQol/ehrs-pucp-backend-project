# EHRS PUCP - Backend Project


**_ENGLISH_**

**Note: This project is in develop (expected completion date: February-March 2021)**

Backend project for EHRS PUCP, a theorical national Electronic Health System in Peru.

This is a thesis project of BsC. Jorge Fatama Vera (role: Backend Develop & System/Testing Analist) & BsC. Katherine Ruiz Quispe (role: Frontend Develop) for Informatic Engineer grade in Pontificia Universidad Católica del Perú with advice from MsC. Angel Lena Valega (thesis advisor for this project).

**_ESPAÑOL_**

**Nota: Este proyecto está en desarrollo (fecha de término prevista: Febrero-Marzo 2021)**

Proyecto de bacjend para EHRS PUCP, un Sistema de Historias Clínicas teórico nacional en Perú.

Este es un proyecto de tesis del Bach. Jorge Fatama Vera (rol: desarrollador backend y analista de sistema/pruebas) y Bach. Katherine Ruiz Quispe (rol: desarrolladora frontend) para el grado de Ingeniero/a Informático en la Pontificia Universidad Católica del Perú con la asesoría del Mag. Angel Lena Valega (asesor de tesis de este proyecto).